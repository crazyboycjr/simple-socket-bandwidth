.PHONY: clean

CFLAGS := -std=c++14 -O3 -Wall
LDFLAGS := -lpthread

APP := client server

all: $(APP)

client: client.cc
	g++ $^ -o $@ $(CFLAGS) $(LDFLAGS)

server: server.cc
	g++ $^ -o $@ $(CFLAGS) $(LDFLAGS)

clean:
	rm -f $(APP)
