#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Usage:  %s <host>\n", argv[0]);
    return 1;
  }
  int err;
  struct sockaddr_in serv_addr;
  int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (sock < 0) {
    perror("socket call failed");
    exit(-1);
  }

  memset(&serv_addr, 0, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(1313);
  serv_addr.sin_addr.s_addr = inet_addr(argv[1]);

  err = connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
  if (err < 0) {
    perror("connect failed");
    exit(-1);
  }

  socklen_t sock_len;
  int sendbuf_size = 0;
  getsockopt(sock, SOL_SOCKET, SO_SNDBUF, &sendbuf_size, &sock_len);
  printf("sendbuf size is %d\n", sendbuf_size);
  sendbuf_size = 365 * 1024;
  setsockopt(sock, SOL_SOCKET, SO_SNDBUF, &sendbuf_size, sock_len);
  printf("sendbuf size is %d\n", sendbuf_size);

  const int SEND_SIZE = 65536;
  char send_buf[SEND_SIZE];
  for (;;) {
    ssize_t n, tlen = 0;
    while ((n = send(sock, send_buf + tlen, SEND_SIZE - tlen, 0)) >= 0) {
      tlen += n;
      if (tlen >= SEND_SIZE)
        break;
    }
  }

  close(sock);
  return 0;
}
