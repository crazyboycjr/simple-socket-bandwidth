#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>
#include <chrono>
#include <thread>
#include <vector>
#include <assert.h>
#include <atomic>
#include <mutex>

using namespace std::chrono;
using Clock = high_resolution_clock;

struct Counter {
  Counter() : cnt{0}, bytes{0}, tp{Clock::now()} {}
  void Add(size_t add) {
    bytes += add;
    if ((++cnt & 0xff) == 0xff) {
      auto now = Clock::now();
      if ((now - tp) >= seconds(1)) {
        duration<double> dura = now - tp;
        printf("Speed: %.6fMB/s\n", bytes / dura.count() / 1048576);
        bytes = 0;
        tp = now;
      }
    }
  }
  int cnt;
  size_t bytes;
  Clock::time_point tp;
};

int GetRecvBufSize(int sock) {
  int size;
  socklen_t opt_len;
  int err = getsockopt(sock, SOL_SOCKET, SO_RCVBUF, &size, &opt_len);
  if (err < 0) {
    perror("getsockopt SO_RCVBUF failed");
    exit(-1);
  }
  return size;

}
int SetRecvBufSize(int sock, int size) {
  int err = setsockopt(sock, SOL_SOCKET, SO_RCVBUF, &size, sizeof(size));
  if (err < 0) {
    perror("setsockopt SO_RCVBUF failed");
    exit(-1);
  }
  return size;
}
void ProcessData(int tid, int sock) {
  int recvbuf_size = SetRecvBufSize(sock, 365 * 1024);
  printf("the recvbuf size is set to %d\n", recvbuf_size);

  Counter counter;

  const int RECV_BUF = 65536;
  char *recv_buf = (char *)malloc(RECV_BUF);
  ssize_t n, recv_bytes = 0;
  while ((n = recv(sock, recv_buf, RECV_BUF, 0)) >= 0) {
    recv_bytes += n;
    counter.Add(n);
  }

  printf("recv error happened");
  free(recv_buf);
  close(sock);
}
int main() {
  int err = 0;
  int listener;
  listener = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (listener < 0) {
    perror("socket call failed");
    exit(-1);
  }

  socklen_t sock_len;
  struct sockaddr_in serv_addr, client_addr;
  memset(&serv_addr, 0, sizeof(serv_addr));

  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(1313);
  serv_addr.sin_addr.s_addr = INADDR_ANY;

  char addr_buf[INET_ADDRSTRLEN];
  printf("Bind on Address %s:%hu\n",
         inet_ntop(AF_INET, &serv_addr.sin_addr, addr_buf, sizeof(addr_buf)),
         ntohs(serv_addr.sin_port));

  sock_len = sizeof(serv_addr);
  err = bind(listener, (struct sockaddr *)&serv_addr, sock_len);
  if (err < 0) {
    perror("bind failed");
    exit(-1);
  }

  err = listen(listener, 512);
  if (err < 0) {
    perror("listen failed");
    exit(-1);
  }

  const int data_thread_num = 16;
  std::vector<std::thread *> data_threads;

  for (int i = 0; i < data_thread_num; i++) {
    int cli_sock;
    cli_sock = accept(listener, (struct sockaddr *)&client_addr, &sock_len);
    assert(cli_sock > 0);
    printf("Accept connection from Address %s:%hu\n",
           inet_ntop(AF_INET, &client_addr.sin_addr, addr_buf, sizeof(addr_buf)),
           ntohs(client_addr.sin_port));

    std::thread *data_thread = new std::thread(ProcessData, i + 1, cli_sock);
    data_threads.push_back(data_thread);
  }

  for (auto t : data_threads)
    t->join();

  close(listener);
  return 0;
}
